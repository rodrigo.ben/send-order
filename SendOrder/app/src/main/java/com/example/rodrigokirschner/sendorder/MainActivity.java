package com.example.rodrigokirschner.sendorder;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //Variáveis Globais
    int text=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buttonConfirm(View v){
        Order order = new Order();
        CheckBox chocolate = (CheckBox)findViewById(R.id.checkChocolate);
        CheckBox chantilly = (CheckBox)findViewById(R.id.checkChantily);
        EditText name = (EditText)findViewById(R.id.editName);
        String message;
        float total=0;
        if(pegaString().equals("")){
            Toast.makeText(this, R.string.texto_alerta_nome_invalido,(Toast.LENGTH_SHORT)).show();
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(500);
            return;
        }
        else{
            order.clientName=name.getText().toString().toUpperCase();
        }
        if(chocolate.isChecked()||chantilly.isChecked()){
            if(chocolate.isChecked()&& !chantilly.isChecked()){
                total=text*4;
                order.orderMessage=getResources().getString(R.string.text_your_order)
                        +" "+text+" "+
                        getResources().getString(R.string.text_coffee)
                +"\n"+getResources().getString(R.string.text_with)+" "
                        + getResources().getString(R.string.texto_chocolate)+" "
                        +getResources().getString(R.string.text_on_value)+total
                        +".\n"+getResources().getString(R.string.text_comming_soon);
            }
            else if(chantilly.isChecked()&& !chocolate.isChecked()){
                total= (float) (text*4.50);
                order.orderMessage=getResources().getString(R.string.text_your_order)
                        +" "+text+" "+
                        getResources().getString(R.string.text_coffee)
                        +"\n"+getResources().getString(R.string.text_with)+" "
                        + getResources().getString(R.string.texto__chantily)+" "
                        +getResources().getString(R.string.text_on_value)+total
                        +".\n"+getResources().getString(R.string.text_comming_soon);
                //coberturasSelecionada.setText(R.string.texto__chantily);

            }
            else if(chantilly.isChecked()&&chocolate.isChecked()){
                total=(float) (text*5.50);
                order.orderMessage=getResources().getString(R.string.text_your_order)
                        +" "+text+" "+
                        getResources().getString(R.string.text_coffee)
                        +"\n"+getResources().getString(R.string.text_with)+" "
                        + getResources().getString(R.string.texto_chocolate)+" "+getResources().getString(R.string.texto_e)+" "+
                        getResources().getString(R.string.texto__chantily)+" "
                        +getResources().getString(R.string.text_on_value)+total
                        +".\n"+getResources().getString(R.string.text_comming_soon);

            }

            else if(!chantilly.isChecked()&&!chocolate.isChecked()){
                // coberturasSelecionada.setText(R.string.texto_sem_cobertura);
                message=R.string.text_your_order+text+R.string.text_coffee+"\n"+R.string.text_on_value+total;
                order.orderMessage=getResources().getString(R.string.text_your_order)+" "+"";
                total=text*3;
            }


        }

        toSent(v, order);

    }



    public String pegaString(){
        EditText entrada= (EditText)findViewById(R.id.editName);
        return entrada.getText().toString();
    }

    public void  toSent(View v, Order order){
        String[] TO = {"geison.camara@digitaldesk.com.br"};
        String[] CC={"rodrigo.ben@digitaldesk.com.br"};
        Intent emailIntent= new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL,TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.text_sr)+" "+order.clientName);
        emailIntent.putExtra(Intent.EXTRA_TEXT, order.orderMessage);
        try{
            startActivity(Intent.createChooser(emailIntent, "Senhor, " + order.clientName));
           finish();
            Log.i("Finished sending mail", "");
        }catch(android.content.ActivityNotFoundException ex){
            Toast.makeText(MainActivity.this,
                    "There is no email client installed.", Toast.LENGTH_SHORT).show();

        }

    }




    @Override
    public void onClick(View v) {
        TextView valor = (TextView)findViewById(R.id.numero);
        int resultado=Integer.parseInt(valor.getText().toString());
        switch (v.getId()){
            case R.id.aumenta:
                resultado++;
                break;
            case R.id.reduz:
                if(resultado>0){
                    resultado--;
                }
                else{
                    Toast.makeText(this, R.string.texto_alerta_numero_negativo_cafe,(Toast.LENGTH_SHORT)).show();
                }
                break;
        }
        text=resultado;
        valor.setText(resultado+"");
    }

}
